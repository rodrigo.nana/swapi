'use strict';
var mongoose = require('mongoose'),
Planets = mongoose.model('Planets');

var swapiPlanetsItegration = require('./../integration/swapiPlanetsItegration')

exports.list_all = function(req, res) {
  Planets.find({}, function(err, planets) {
    if (err)
      res.send(err);
    let swapi = new swapiPlanetsItegration.SwapiPlanetsItegration();
    swapi.get(planets, (function(object){
                          res.json(object);  
                        }));
  });
};

exports.create = function(req, res) {
  var new_planet = new Planets(req.body);
  new_planet.save(function(err, planet) {
    if (err)
      res.send(err);
    res.json(planet);
  });
};

exports.read = function(req, res) {
  Planets.findById(req.params.planetId, function(err, planet) {
    if (err)
      res.send(err);
    console.log(planet)
    res.json(planet);
  });
};

exports.update = function(req, res) {
  Planets.findOneAndUpdate({_id: req.params.planetId}, req.body, {new: true}, function(err, planet) {
    if (err)
      res.send(err);
    res.json(planet);
  });
};

exports.delete = function(req, res) {
  Planets.remove({
      _id: req.params.planetId
    }, function(err, planet) {
      if (err)
        res.send(err);
      res.json({ message: 'Planets successfully deleted' });
  });
};

