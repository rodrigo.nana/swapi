'use strict';
module.exports = function(app) {
  var planets = require('../controllers/planetsController');
  app.route('/planets')
  	.get(planets.list_all)
    .post(planets.create);
    
  app.route('/planets/:planetId')
    .get(planets.read)
    .put(planets.update)
    .delete(planets.delete);
};