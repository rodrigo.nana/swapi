'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PlanetSchema = new Schema({
  name: {
    type: String,
    required: 'Name is required'
  },
  terrain: {
    type: String,
    required: 'Terrain is required'
  },
  climate: {
    type: String,
    required: 'Climate is required'
  },
  Created_date: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model('Planets', PlanetSchema);